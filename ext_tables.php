<?php

if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

/** Include the class tx_extbibsonomy_common, so we can call functions from this class in 
 *  the flexform.xml **/
include_once(t3lib_extMgm::extPath($_EXTKEY).'class.tx_extbibsonomy_common.php');

t3lib_div::loadTCA("tt_content");
t3lib_extMgm::addTCAcolumns("tt_content",$tempColumns,1);

t3lib_div::loadTCA('tt_content');

$TCA['tt_content']['types']['list']['subtypes_excludelist'][$_EXTKEY.'_pi1']='layout,select_key';

/** Add the Flexform. It describes the backend-formular, where users can input their settings **/
$TCA['tt_content']['types']['list']['subtypes_addlist'][$_EXTKEY.'_pi1'] ='pi_flexform';
t3lib_extMgm::addPiFlexFormValue($_EXTKEY.'_pi1', 'FILE:EXT:'.$_EXTKEY . '/flexform.xml');
										
t3lib_extMgm::addPlugin(array('LLL:EXT:ext_bibsonomy/locallang_db.xml:tt_content.list_type_pi1', $_EXTKEY.'_pi1'),'list_type');


t3lib_extMgm::addStaticFile($_EXTKEY,"pi1/static/","Bibsonomy Plugin");


if (TYPO3_MODE=="BE")	$TBE_MODULES_EXT["xMOD_db_new_content_el"]["addElClasses"]["tx_extbibsonomy_pi1_wizicon"] = t3lib_extMgm::extPath($_EXTKEY).'pi1/class.tx_extbibsonomy_pi1_wizicon.php';
?>