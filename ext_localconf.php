<?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

/** add a hook which calls a function on saving formdata in the backend **/
require_once(t3lib_extMgm::extPath($_EXTKEY).'class.tx_extbibsonomy_common.php');

/** 
 * Including the HOOK, this is a function which is called before saving the form-data
 *  in the backend. It is used for updating the layouts from BibSonomy 
 * **/
 
$ref = 'EXT:' . $_EXTKEY . '/user_tx_extbibsonomy_postFunc.php:user_tx_extbibsonomy_postFunc';
$GLOBALS ['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass'][] = $ref;

  ## Extending TypoScript from static template uid=43 to set up userdefined tag:
t3lib_extMgm::addTypoScript($_EXTKEY,'editorcfg','
	tt_content.CSS_editor.ch.tx_extbibsonomy_pi1 = < plugin.tx_extbibsonomy_pi1.CSS_editor
',43);


t3lib_extMgm::addPItoST43($_EXTKEY,'pi1/class.tx_extbibsonomy_pi1.php','_pi1','list_type',0);
?>