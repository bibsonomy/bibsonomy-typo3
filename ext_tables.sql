#
# Table that stores layouts from bibsonomy
#
CREATE TABLE tx_extbibsonomy_layouts (
	path tinytext NOT NULL,
	displayName tinytext NOT NULL,
	lastUpdate TIMESTAMP NOT NULL
);
