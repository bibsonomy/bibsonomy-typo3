<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2008 Michael Wagner <michi83@t-online.de>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

require_once(PATH_tslib.'class.tslib_pibase.php');
require_once(t3lib_extMgm::extPath('ext_bibsonomy').'class.tx_extbibsonomy_common.php');
/**
 * Plugin 'Bibsonomy Frontend Plugin' for the 'ext_bibsonomy' extension.
 *
 * @author	Michael Wagner <michi83@t-online.de>
 * @package	TYPO3
 * @subpackage	tx_extbibsonomy
 */
class tx_extbibsonomy_pi1 extends tslib_pibase {
	var $prefixId      = 'tx_extbibsonomy_pi1';		// Same as class name
	var $scriptRelPath = 'pi1/class.tx_extbibsonomy_pi1.php';	// Path to this script relative to the extension dir.
	var $extKey        = 'ext_bibsonomy';	// The extension key.
	
	//DISABLE CACHING
	//var $pi_checkCHash = true;
	
	/**
	 * The main method of the PlugIn
	 *
	 * @param	string		$content: The PlugIn content
	 * @param	array		$conf: The PlugIn configuration
	 * @return	The content that is displayed on the website
	 */
	function main($content,$conf)	{
		$this->conf=$conf;
		$this->pi_setPiVarDefaults();
		$this->pi_loadLL();
		//$this->id=$GLOBALS['TSFE']->id;
		
		//the id of the current content element where this plugin is used, the id is unique		
		$this->uid = $this->cObj->data['uid'];
		//DISABLE CACHING
		$this->pi_USER_INT_obj = 1;
					
		/** 
		 * get user input from flexform
		 **/
		$this->pi_initPIflexForm(); // Init and get the flexform data of the plugin
		$flexContent = array();
		$piFlexForm = $this->cObj->data['pi_flexform'];
		
		/** Read data from all fields an put it into flexContent **/
		foreach ($piFlexForm['data'] as $sheet => $data) {
			foreach ( $data as $lang => $value ) {
				foreach ( $value as $key => $val ) {
               		$flexContent[$key] = $val['vDEF'];
           		}
			}
		}
		
		$common = new tx_extbibsonomy_common();
		
		//remove slashes at the end of the path
		$requContent = $common->checkPath($flexContent['bib_content']);		
		
		/*
		 * FILTER IS NOT IN USE, DUE TO PROBLEMS WITH AUTOMATIC SYSTEM-TAG ADDING
		 * 
		if($flexContent['bib_enable_filter']){
			if(strlen($flexContent['bib_entryType_value']) > 0){
				$requContent.='+'.$flexContent['bib_entryType'].':'.$flexContent['bib_entryType_value'];
			}
		}*/
		
		//show publications from bibsonomy in a given layout
		if($flexContent['bib_include_publications']){			
											
			$requContent = $this->setParam($requContent,"items",$flexContent['bib_publication_count']);
								
			//do http request		
			$publications = $this->getPageContent($flexContent['bib_server_url'],$flexContent['bib_layout_type'],$requContent,$flexContent['bib_login_name'],$flexContent['bib_user_password']);

			$publications = "<td style='vertical-align:top'>
								<table>
									<tr> 
										<td>".$publications."</td>
									</tr> 
								</table>
							</td>";
		}

		//show the tagcloud received by a given $url
		if($flexContent['bib_include_tagcloud']){
			//The path to the requested resource
			$tagPath = "/json/tags";

			if($flexContent['bib_show_relatedtags']){
				$tagPath = "/json/relatedtags";
			}		

			if($flexContent['bib_tagcloud_size'] == "" || $flexContent['bib_tagcloud_size'] == "0"){
				$flexContent['bib_tagcloud_size'] = "100";
			}
						
			$tagCloud = $this->buildTagCloud($this->getPageContent($flexContent['bib_server_url'],$tagPath,$requContent,$flexContent['bib_login_name'],$flexContent['bib_user_password']),$flexContent['bib_server_url'],$requContent,$flexContent['bib_tagcloud_size']);
			
			$tagCloud = "<td style='vertical-align:top'>
								<table>
									<tr> 
										<td>".$tagCloud."</td>
									</tr> 
								</table>
							</td>";
		}

		//write mainContent using blind tables (ugly but efficient)
		$mainContent = "<table style='width:100%'>
							<tr>".$publications.$tagCloud."</tr>
						</table>";
		
		//display meta informations at the bottom of the page
		if($flexContent['bib_show_metadata']){
			$mainContent.="<br/><br/>".$common->writeMetaInformation($flexContent['bib_server_url'].$flexContent['bib_layout_type'].$requContent);				
		}	
						
		//convert to ISO?
		if($flexContent['bib_decode_iso']){
			$mainContent = utf8_decode($mainContent);
		}

		//return data
		return $this->pi_wrapInBaseClass($mainContent);
	}
	
	/**
	 * Method executes a http request and
	 * returns the content of the requested url encoded in iso or utf
	 *
	 * The input parameters build an url string as follows: $url+$layout+$sitecontent
	 *
	 * $url: Server adress
	 * $layout: The export layout
	 * $sitecontent: displayed content
	 * $username: The login-name of a registered user
	 * $decode: true or false, convert data with iso
	 * $password: The user-password 
	 *
	 */
	function getPageContent($url,$layout,$siteContent,$userName,$password){

		//build url
		$finalURL = $url.$layout.$siteContent;
		//create a global common object, so we can use helper functions from this class
		$common = new tx_extbibsonomy_common();

		/**$data = array();
		for($i = 0; $i < 10; $i++){
			$data[$i] = $common->makeHttpRequest($finalURL,$userName,$password);
		}	
		
		for($i = 0; $i < 9; $i++){
			if(strcmp($data[$i],$data[$i++]) != 0){
				echo "NOT EQUAL :-(";
			}else{
				echo "EQUAL :-)";
			}
		}**/
		
		//make http request
		$response = $common->makeHttpRequest($finalURL,$userName,$password,true);
		return $response;		
	}
	
	/** 
	 * Build a tagcloud
	**/
	function buildTagCloud($content,$host,$link,$size){
		
		$jsonContent = json_decode($content,true);
		
		//only continue if json output exists
		if(is_array($jsonContent['items']) == false){
			return "";		
		}

		//create a global common object, so we can use helper functions from this class
		$common = new tx_extbibsonomy_common();

		//get the maximum tagcount
		$maxTagCount = $common->calculateMaxCount($jsonContent['items']);
		
		//sort the array
		usort($jsonContent['items'],array(&$this,'myComparison'));
		
		//set max size 100
		$newArray = $common->shrinkArray($jsonContent['items'],$size);
		sort($newArray);
		
		//tags will be shown in a list element		
		$tagCloud = "<ul>";
		foreach($newArray as $item){		
			$count = $item['count'];	
			$tag_style = $this->calculateTagSize($count, $maxTagCount);
			$href = $this->buildTagPath($link,"/".$item['label']);
			$tagCloud.= "<li style='display:inline'>
					<a style='".$tag_style."' href='".$host.$href."' target='blank'>".$item['label']."</a>
				     </li>";				
		}
		$tagCloud.="</ul>";
		return $tagCloud;
	}

	/** 
	 * calculate the tag size and return a stylesheet
	 */
	function calculateTagSize($count, $max){
		$tiny = "font-size:85%";
		$normal = "font-size:95%";
		$large = "font-size:105%";
		$huge = "font-size:115%";		

		if($count == 0 || $max == 0){
			return $tiny;
		}

		$percentage = (($count*100)/$max);
		if($percentage < 25){
			return $tiny;
		}else if($percentage >= 25 && $percentage < 50){
			return $normal;
		}else if($percentage >= 50 && $percentage < 75){
			return $large;
		}else if($percentage >= 75){		
			return $huge;
		}
	}

	/** 
	 * build the link which stands behind each tag
	 */
	function buildTagPath($path,$ext){
		
		$newPath = "";
		//look if the link already contains a tag and when yes, cut it off
		if(count(explode("/",$path)) >= 4){				
			$path = substr($path,0,(strrpos($path,"/")));
		}
		//does the link contain parameters ?
		if(stristr($path,"?") == true){
			$split = explode("?",$path);					
			$path = $split[0];
		}

		$newPath = $path.$ext;
		return $newPath;
	}
	
	/** 
	 * compare function for usort, this is needfull for displaying the first 100 tags
	 */
	function myComparison(&$a, &$b){
		return ($a['count'] == $b['count']) ? 0 : (($a['count'] < $b['count']) ? 1 : -1);
	}
	
	/** Function adds or overwrites a parameter in an URL **/
	function setParam($url, $paramName, $paramValue){
		
		if(preg_match("/.*([&\\?])".$paramName."\\=[^&#$]+.*/",$url)){
			return preg_replace("/([&\\?])".$paramName."\\=[^&#$]+/","$1".$paramName."=".$paramValue,$url);
		}
		if(preg_match("/.*\\?.*/",$url)){
			return $url."&".$paramName."=".$paramValue;
		}
		
		return $url."?".$paramName."=".$paramValue;
	}		
}



if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/ext_bibsonomy/pi1/class.tx_extbibsonomy_pi1.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/ext_bibsonomy/pi1/class.tx_extbibsonomy_pi1.php']);
}

?>
