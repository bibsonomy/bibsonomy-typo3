<?php

require_once(t3lib_extMgm::extPath('ext_bibsonomy').'HTTP/Request2.php');


/**
 * This class contains some functions that are used in the flexform.
 *
 * @author	Michael Wagner <michi83@t-online.de>
 * @package	TYPO3
 * @subpackage	tx_extbibsonomy
 */

class tx_extbibsonomy_common{
	var $extKey        = 'ext_bibsonomy';	// The extension key.
	var $scriptRelPath = 'class.tx_extbibsonomy_common.php';	// Path to this script relative to the extension dir.
			
	/**
	 * Fill the field with all available layouts. 
	 * If no layouts available, add some default layouts
	 */
	function user_getLayouts($config){
		
		if(!$this->checkLayouts()){
			$config['items'][0] = Array('HTML', '/layout/html');
			$config['items'][1] = Array('CUSTOM', '/layout/custom');
		}else{
			$qry = $this->queryForLayouts();
			$i = 0; 
			while($row = mysql_fetch_array($qry)){
				$config['items'][$i++] = Array($row['displayName'], $row['path']);
			}
		}
		return $config;
	}
	
	/**
	 * show add a message if no layouts are found in database
	 */
	function user_showMessage(){
		if(!$this->checkLayouts()){
			return '<p style="color:red;">Note: More layouts will be available after the first saving!</p>';
		}
		return;
	}
	
	/** 
	* Method checks if there are layouts available, if not load them from BibSonomy. 
	* Furthermore we look how many days have elapsed, since the last update was proceeded.
	* If more than 7 days have elapsed, layouts will be updated automatically. 
	* **/
	function autoUpdate($server_url){		
				
		if(!$this->checkLayouts()){
			//first insert
			$this->updateLayouts($server_url);
		}else{
			$content = mysql_fetch_array($this->queryForLayouts());
			//look if more than 7 days have elapsed since last update (604800sec = 7days)
			if(time() - strtotime($content['lastUpdate']) > 604800){
				//update layouts
				$this->updateLayouts($server_url);
			}
		}
	}
	
	/**
	 * Look if layouts are stored in the database and return true or false
	 */
	function checkLayouts(){
		//no content in table tx_extbibsonomy_layouts?
		$content = mysql_fetch_array($this->queryForLayouts());
		if(!$content){
			return false;
		}
		return true;
	}
	
	/**
	 * Function receives all layouts from bibsonomy
	 * and writes them into table tx_extbibsonomy_layouts
	 */
	function updateLayouts($server_url){
		
		//receive layouts from bibsonomy		
		$response = $this->makeHttpRequest($server_url.'/layoutinfo?formatEmbedded=true','','',false);
		//everything ok?
		if($response != null){
			$json = json_decode($response,true);
			
			$jsonData = $json['layouts'];
					
			// receiving layouts successfull?
			if(is_array($jsonData)){
				
				//first delete all records in table tx_extbibsonomy_layouts
				$this->clearLayouts();
				
				//add the custom layout
				$custom = array('displayName' => 'CUSTOM', 'path' => '/layout/custom'); 
				$this->insertLayout($custom);
				
				//insert each layout in tx_extbibsonomy_layouts
				$i = 1;
				foreach($jsonData as $layouts => $layout){
					/** only save layouts with mimeType = text/html **/
					if($layout['mimeType'] == 'text/html'){
						//we only need the displayName, from this value we build a subpath of the url
						$insert_array = array('displayName' => $layout['displayName'], 'path' => '/layout/'.$layout['name']);
						$this->insertLayout($insert_array);
						$i++;
					}
				}
				//make some output
				echo "inserted ".$i." records!";
			}
		//update failed? make some error output
		}else{
			echo "<h4>UPDATING LAYOUTS FAILED</h4>Please check the server url: ".$server_url."/layoutinfo";
		}
	}	
		
	/** Method executes a MySQL query and returns the result **/
	function queryForLayouts(){
		$result = $GLOBALS['TYPO3_DB']->exec_SELECTquery('path,displayName,lastUpdate',
							      'tx_extbibsonomy_layouts','','','','');
							      
		return $result;
	}
		
	/**
	 * Function returns entry-types to tce-form 'filter'
	 */
	function user_getEntryTypes($config){
		$types = Array('EntryType' => 'sys:entrytype','Year' => 'sys:year','Author' => 'sys:author','User' => 'sys:user','Group' => 'sys:group');
		foreach($types as $type => $value){
			$config['items'][$i++] = Array($type,$value);
		}
	
		return $config;
	}					
	/** 
	 * Function makes a http request and returns the response using Pear HTTP_Request2.
	**/
	function makeHttpRequest($url,$username,$password,$errmsg){
		$request = new HTTP_Request2($url,HTTP_Request2::METHOD_GET);
		$request->setAuth($username, $password, HTTP_Request2::AUTH_BASIC);
		try{
			$response = $request->send();	
			
			if($response->getStatus() == 200){
				return $response->getBody();
			}else{
				if($errmsg){
					//return a short error message
					echo "<h2>Input failure!</h2><br/>
					   		Your entries generate following url:  <a href='".$url."' target='blank'>".$url."</a>.<br/><br/>
					   		For further details, look at: 
				           	<a href='http://www.bibsonomy.org/help/basic/semantics/' target='blank'>http://www.bibsonomy.org/help/basic/semantics</a>.";
				}
			    return;
			}
			
			
		}catch (HTTP_Request2_Exception $e) {
    		echo 'Error: ' . $e->getMessage();
    		return;
		}
	}
	
	/**
	 * Insert a record in tx_extbibsonomy_layouts
	 */
	function insertLayout($insert_array){
		$GLOBALS['TYPO3_DB']->exec_INSERTquery('tx_extbibsonomy_layouts',$insert_array);
	}
	
	/**
	 * Delete all records in table tx_extbibsonomy_layouts
	 */
	function clearLayouts(){
		$GLOBALS['TYPO3_DB']->exec_DELETEquery('tx_extbibsonomy_layouts','');	
	}
	
	/** 
	 * shrink the size of an array to $size 
	 * **/ 
	function shrinkArray($arr, $size){
		if(count($arr) <= $size){
			return $arr;
		}else{
			while(count($arr) > $size){
				array_pop($arr);			
			}
		}	
		return $arr;
	}
	
	/**
	 * read some metainformations from ext_emconf.php 
	 */
	function writeMetaInformation($path){

		@include_once(t3lib_extMgm::extPath($this->extKey, 'ext_emconf.php'));
		
		$metaString = "URL: ".$path."<br/>";
		$metaString.= "Content generated by ".$EM_CONF[$extKey]['title']." - Version ".$EM_CONF[$extKey]['version'].".<br/>";
		$metaString.= "Author: ".$EM_CONF[$extKey]['author']." (".$EM_CONF[$extKey]['author_email'].").";

		return "<div style='font-size:70%;font-style:italic;'>".$metaString."</div>";
	}
	
	/** 
	 * remove (if extist) a slash at the end of a bibsonomy url
	 */ 
	function checkPath($path){
		if(strrpos($path,"/") == (strlen($path)-1)){		
			$path = substr($path,0,strrpos($path,"/"));
		}
		if(strpos($path,"/") != 0){
			$path = "/".$path;
		}
		return $path;
	}
	
	/**
	 * calculate the item (tag) with the maximum count, neccessary for sorting
	 */
	function calculateMaxCount($items){
		$maxCount = 0;
		foreach($items as $item){	
			$count = $item['count'];
			if($count > $maxCount){
				$maxCount = $count;			
			}
		}
		return $maxCount;
	}	
}	

if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/ext_bibsonomy/class.tx_extbibsonomy_common.php']) {
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/ext_bibsonomy/class.tx_extbibsonomy_common.php']);
}

?>