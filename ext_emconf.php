<?php

########################################################################
# Extension Manager/Repository config file for ext "ext_bibsonomy".
#
# Auto generated 19-02-2010 13:02
#
# Manual updates:
# Only the data in the array - everything else is removed by next
# writing. "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Bibsonomy Extension',
	'description' => 'Bibsonomy Extension',
	'category' => 'plugin',
	'shy' => 0,
	'version' => '2.0.2',
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'loadOrder' => '',
	'module' => '',
	'state' => 'beta',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearcacheonload' => 0,
	'lockType' => '',
	'author' => 'Michael Wagner',
	'author_email' => 'michi83@t-online.de',
	'author_company' => '',
	'CGLcompliance' => '',
	'CGLcompliance_note' => '',
	'constraints' => array(
		'depends' => array(
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'_md5_values_when_last_written' => 'a:32:{s:9:"ChangeLog";s:4:"e7db";s:10:"README.txt";s:4:"ee56";s:32:"class.tx_extbibsonomy_common.php";s:4:"ff12";s:12:"ext_icon.gif";s:4:"1bdc";s:17:"ext_localconf.php";s:4:"9885";s:14:"ext_tables.php";s:4:"0102";s:14:"ext_tables.sql";s:4:"d93b";s:12:"flexform.xml";s:4:"8c6d";s:13:"locallang.xml";s:4:"c1e4";s:16:"locallang_db.xml";s:4:"7890";s:33:"user_tx_extbibsonomy_postFunc.php";s:4:"b0d9";s:17:"HTTP/Request2.php";s:4:"38df";s:17:"HTTP/Net/URL2.php";s:4:"fbca";s:25:"HTTP/Request2/Adapter.php";s:4:"aec0";s:22:"HTTP/Request2/Curl.php";s:4:"eb11";s:27:"HTTP/Request2/Exception.php";s:4:"0158";s:21:"HTTP/Request2/Log.php";s:4:"7a3b";s:22:"HTTP/Request2/Mock.php";s:4:"0859";s:31:"HTTP/Request2/MultipartBody.php";s:4:"cad0";s:26:"HTTP/Request2/Response.php";s:4:"c2cf";s:24:"HTTP/Request2/Socket.php";s:4:"a6f7";s:32:"HTTP/Request2/PEAR/Exception.php";s:4:"6894";s:14:"doc/manual.sxw";s:4:"d162";s:19:"doc/wizard_form.dat";s:4:"09e1";s:20:"doc/wizard_form.html";s:4:"f6d7";s:13:"pi1/Thumbs.db";s:4:"340e";s:14:"pi1/ce_wiz.gif";s:4:"02b6";s:33:"pi1/class.tx_extbibsonomy_pi1.php";s:4:"c89e";s:41:"pi1/class.tx_extbibsonomy_pi1_wizicon.php";s:4:"134a";s:13:"pi1/clear.gif";s:4:"cc11";s:17:"pi1/locallang.xml";s:4:"8f2f";s:24:"pi1/static/editorcfg.txt";s:4:"5c46";}',
	'suggests' => array(
	),
);

?>