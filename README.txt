DEPRECATION NOTE

Please use https://bitbucket.org/bibsonomy/bibsonomy-typo3-csl (supports up-to-date Typo3 versions and adds CSL support to the plugin).

This is the BibSonomy-Plugin for Typo3. It enables you to include data from
the social publication and bookmark sharing platform www.bibsonomy.org on 
a Typo3 page.

See http://www.bibsonomy.org/help/tools/typo3_etension for a detailed documentation
of how to install and configure the plugin.