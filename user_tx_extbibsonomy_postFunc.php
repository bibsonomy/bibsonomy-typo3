<?

require_once(t3lib_extMgm::extPath('ext_bibsonomy').'class.tx_extbibsonomy_common.php');

/**
 * This class loads new layouts from www.bibsonomy.org/exportLayouts and stores them in the typo3 database
 *
 * @author	Michael Wagner <michi83@t-online.de>
 * @package	TYPO3
 * @subpackage	tx_extbibsonomy
 */

class user_tx_extbibsonomy_postFunc{
	var $extKey        = 'ext_bibsonomy';	// The extension key.
	var $scriptRelPath = 'user_tx_extbibsonomy_postFunc.php';	// Path to this script relative to the extension dir.
	
	/**
	 * Before saving data, look if user wants to update layouts from Bibsonomy
	 */
	function processDatamap_preProcessFieldArray(&$fieldArray, $table, $id, &$pObj) {
		//check if the checkbox 'update layouts' is set
		$var = $fieldArray['pi_flexform']['data']['bib_layout']['lDEF']['bib_update_layouts']['vDEF'];
		
		//get the server url from the flexform
		$server_url = $fieldArray['pi_flexform']['data']['bib_misc']['lDEF']['bib_server_url']['vDEF'];
		
		$common = new tx_extbibsonomy_common();
		//if checkbox and server-url are set
		if($var && strlen($server_url) > 0){
			$common->updateLayouts($server_url);
		}else if(strlen($server_url) > 0){
			$common->autoUpdate($server_url);
		}
		
		//finally unset the checkbox
		$fieldArray['pi_flexform']['data']['bib_layout']['lDEF']['bib_update_layouts']['vDEF'] = 0;
	}
		
}

if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/ext_bibsonomy/user_tx_extbibsonomy_postFunc.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/ext_bibsonomy/user_tx_extbibsonomy_postFunc.php']);
}

?>